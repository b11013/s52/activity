import { Container } from "react-bootstrap";
import AppNavBar from "./components/AppNavBar";
import "./App.css"; //from React
import Home from "./pages/Home";
import Courses from "./pages/Courses";
import Register from "./pages/Register";
import Login from "./pages/Login";

function App() {
  return (
    <>
      <AppNavBar />
      <Container>
        <Home />
        <Courses />
        <Register />
        <Login />
      </Container>
    </>
  );
}

export default App;

// if its from react u need closing pair tags
//if its from component it is self closing.
