import { useState, useEffect } from "react";
import { Row, Col, Card, Button } from "react-bootstrap";

export default function CourseCard({ props }) {
  //   console.log(props);
  //   console.log(typeof props);
  //   destructuring
  const { name, description, price } = props;
  // console.log(name);
  //SYNTAX of usestate
  // const [getter, setter] = useState(initialValueOfGetter)

  //ACTIVITY s51
  const [count, setCount] = useState(0);
  const [seatCount, setSeatCount] = useState(30);
  const [isOpen, setIsOpen] = useState(false);

  const enroll = () => {
    // console.log(`Enrollees: ${count}`);
    if (seatCount > 0) {
      setCount(count + 1);
      setSeatCount(seatCount - 1);
    } else if (seatCount === 0) {
      //alert("No more seats available!");
      //   document.querySelector(".enrollButton").setAttribute("disabled", true);
    }
  };

  useEffect(() => {
    if (seatCount === 0) {
      setIsOpen(true);
    }
  }, [seatCount]);

  return (
    <Row className="mt-3 mb-3">
      {/* <Col xs={12} md={4}> */}
      <Col>
        <Card className="courseCardHighlight p-3">
          <Card.Body>
            <Card.Title>
              <Card.Title> {name} </Card.Title>
            </Card.Title>
            <Card.Subtitle> Description</Card.Subtitle>
            <Card.Text>{description}</Card.Text>
            <Card.Subtitle>Price</Card.Subtitle>
            <Card.Text>{price}</Card.Text>
            <Card.Text>Enrollees: {count}</Card.Text>
            <Card.Text>Seats: {seatCount}</Card.Text>
            <Button
              className="enrollButton"
              variant="primary"
              onClick={enroll}
              disabled={isOpen}
            >
              Enroll
            </Button>
          </Card.Body>
        </Card>
      </Col>
    </Row>
  );
}
